import React from 'react';
import Image from 'next/image';

const schools = [
  {
    name: 'National Institute of Technology (NIT) Jamshedpur',
    major: 'B.Tech (Hons) in Computer Science and Engineering',
    duration: 'Aug 2019 - May 2023',
    location: 'Jamshedpur, Jharkhand, India',
    gpa: '8.69 / 10',
    coursework:
      'Data Structures and Algorithms, Database Management System, Computer Networks, Operating Systems, Theory of Computation, Software Engineering, Object Oriented System Design, Data Mining and Data Ware Housing, Compiler Design, Cloud Computing, Image Processing, Artificial Intelligence, Deep Learning, Blockchain and its Applications',
    description:
      'I graduated with honors and was the Technical Secretary of the Programming Club.',
    image: '/nitjamshedpur.png',
  },
];

const EducationSection = () => {
  return (
    <section id='education'>
      <h1 className='text-center font-bold text-4xl'>
        Education
        <hr className='w-6 h-1 mx-auto my-4 bg-teal-500 border-0 rounded'></hr>
      </h1>

      <div className='flex flex-col space-y-28 md:mb-56'>
        {schools.map((school, idx) => {
          return (
            <div key={idx}>
              <div className='flex flex-col space-y-10 items-stretch justify-center align-top md:space-x-10 md:space-y-0 md:p-4 md:flex-row md:text-left'>
                <div className='flex items-center justify-center md:w-1/4'>
                  <Image
                    src={school.image}
                    alt=''
                    width={250}
                    height={250}
                    className='rounded-full shadow-xl hover:opacity-70'
                  />
                </div>
                <div className='text-center mt-8 md:w-3/4 md:text-left'>
                  <h1 className='text-3xl font-bold mb-6'>{school.name}</h1>
                  <div>
                    <p className='text-sm text-gray-500 dark:text-gray-400'>
                      Major: {school.major} | {school.duration}
                    </p>
                    <p className='text-sm text-gray-500 dark:text-gray-400'>
                      Location: {school.location}
                    </p>
                  </div>
                  <div className='grid gap-4 pt-4'>
                    <div className='space-y-4'>
                      <p>{school.description}</p>
                    </div>
                    <div className='space-y-4'>
                      <p>Coursework: {school.coursework}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </section>
  );
};

export default EducationSection;
